.data
# Matriz de adyacencia expresado en una fila (-1 significa fin de la matriz)
grafo1a:	.word	0, 7, 9, 0, 0, 14, 7, 0, 10, 15, 0, 0, 9, 10, 0, 11, 0, 2, 0, 15, 11, 0, 6, 0, 0, 0, 0, 6, 0, 9, 14, 0, 2, 0, 9, 0, -1
# Cantidad de grafos de la matriz (cada cuanto debo cortar la fila grafo1a)
grafo1b:	.word	6

# Listas auxiliares. Cambiar segundo argumento de cada Lista (por defecto: 6) por la Cantidad de Grafos indicada en la palabra anterior (por defecto: grafo1b)
listaf:		.word	-1:6
listag:		.word	-1:6
listah:		.word	-1:6

nodosAbiertos:	.word	-1:6
nodosCerrados:	.word	-1:6

sucesores:	.word	-1:6
camino:		.word	-1:6


# Especifica nodo de inicio y de fin del algoritmo (partiendo en 1)
nodoInicio:	.word	3
nodoFin:	.word	6


# Otros grafos predise�ados, al usar, cambiar el argumento de largo de las Listas por el indicado en cada palabra
grafo2a:	.word	0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, -1
grafo2b:	.word	6

grafo3a:	.word	1, 2, 0, 4, 34, 2, 0, 1, 11, 0, 0, 2, 4, 5, 6, 1, -1
grafo3b:	.word 	4

grafo4a:	.word	0, 1, 0, 2, 5, 0, 0, 1, 3, -1
grafo4b:	.word	3

# Constantes del programa
espacio:	.asciiz " "
newline:	.asciiz "\n"
errortext:	.asciiz "ERROR!"
testingtext:	.asciiz	"prueba"
infinito:	.word	999999

.text
# Cargar grafo, nodo inicio y nodofin
la $a0, grafo2a
la $a1, grafo2b
lw $a2, nodoInicio
lw $a3, nodoFin

# Ejecutar BFS
jal BFS

# Obtener retorno e imprimirlo
move $a0, $v0
lw $a1, 0($s1)
jal imprimirNodos

# Finalizar programa
j exit

##################### BFS #######################
# Funcion de Best First Search, basado en el algoritmo A*
 
# Entradas:
# $a0: Direccion base del vector
# $a1: Tama�o del vector
# $a2: Nodo de entrada
# $a3: Nodo de salida

# Salidas:
# $v0: Arreglo con el camino �ptimo
#################################################
BFS:
	move $s0, $a0		# $s0 es un puntero fijo a la posicion inicial de la matriz
	move $s1, $a1		# $s1 es la cantidad de nodos (puntero)
	move $s2, $a2		# $s2 es el nodo desde cual parte la busqueda (directo)
	move $s3, $a3		# $s3 es el nodo al cual queremos llegar (directo)
	
bfsAllocate:
	la $t0, nodosAbiertos
	la $t1, nodosCerrados
	la $t2, listaf		# almacena el atributo 'f' de cada nodo
	la $t3, listag		# almacena el atributo 'g' de cada nodo
	la $t4, listah		# almacena el atributo 'h' de cada nodo
	la $t5, camino		# almacena el camino final alcanzado por el algortimo
	
	move $t6, $s2		# $t6 es el nodo 'actual' (ver informe)
	
	sw $t6, 0($t0)		# actual = nodo de inicio
	
	addi $t7, $s2, -1
	sll $t7, $t7, 2
	add $t9, $t2, $t7
	li $t8, 0
	sw $t8, 0($t9)		# actual.f = 0
	add $t9, $t3, $t7
	li $t8, 0
	sw $t8, 0($t9)		# actual.g = 0
	add $t9, $t4, $t7
	li $t8, 0
	sw $t8, 0($t9)		# actual.h = 0
	
bfsWhileCheck:			# variables $t7-$t9 trabajables
	addi $sp, $sp, -36
	sw $ra, 32($sp)
	sw $t6, 28($sp)
	sw $t5, 24($sp)
	sw $t4, 20($sp)
	sw $t3, 16($sp)
	sw $t2, 12($sp)
	sw $t1, 8($sp)
	sw $t0, 4($sp)
	
	move $a0, $t0
	move $a1, $s1
	move $a2, $t2
	jal encontrarMenorAbierto

	lw $t0, 4($sp)
	lw $t1, 8($sp)
	lw $t2, 12($sp)
	lw $t3, 16($sp)
	lw $t4, 20($sp)
	lw $t5, 24($sp)
	lw $t6, 28($sp)
	lw $ra, 32($sp)
	addi $sp, $sp, 36
	
	move $t6, $v0		# actual = nodo abierto con menor 'f'
	bne $t6, $s3, bfsWhile	# si el nodo actual no es la meta, entramos en el while
	j bfsExit		# sino, terminamos
bfsWhile:
	addi $sp, $sp, -36
	sw $ra, 32($sp)
	sw $t6, 28($sp)
	sw $t5, 24($sp)
	sw $t4, 20($sp)
	sw $t3, 16($sp)
	sw $t2, 12($sp)
	sw $t1, 8($sp)
	sw $t0, 4($sp)
	
	move $a0, $t0
	move $a1, $t6
	move $a2, $s1
	jal extraerElemento		# se saca 'actual' de la lista de nodos abiertos

	lw $t0, 4($sp)
	lw $t1, 8($sp)
	lw $t2, 12($sp)
	lw $t3, 16($sp)
	lw $t4, 20($sp)
	lw $t5, 24($sp)
	lw $t6, 28($sp)
	lw $ra, 32($sp)
	addi $sp, $sp, 36
	
	move $t0, $v0
	
	addi $sp, $sp, -36
	sw $ra, 32($sp)
	sw $t6, 28($sp)
	sw $t5, 24($sp)
	sw $t4, 20($sp)
	sw $t3, 16($sp)
	sw $t2, 12($sp)
	sw $t1, 8($sp)
	sw $t0, 4($sp)
	
	move $a0, $t1
	move $a1, $t6
	move $a2, $s1
	jal guardarElemento		# se guarda 'actual' en la lista de nodos abiertos

	lw $t0, 4($sp)
	lw $t1, 8($sp)
	lw $t2, 12($sp)
	lw $t3, 16($sp)
	lw $t4, 20($sp)
	lw $t5, 24($sp)
	lw $t6, 28($sp)
	lw $ra, 32($sp)
	addi $sp, $sp, 36
	
	move $t1, $v0
	
	addi $sp, $sp, -36
	sw $ra, 32($sp)
	sw $t6, 28($sp)
	sw $t5, 24($sp)
	sw $t4, 20($sp)
	sw $t3, 16($sp)
	sw $t2, 12($sp)
	sw $t1, 8($sp)
	sw $t0, 4($sp)
	
	move $a0, $s0
	move $a1, $s1
	la $a2, sucesores
	move $a3, $t6
	jal generarSucesores		# Se generan los sucesores de 'actual'
	
	lw $t0, 4($sp)
	lw $t1, 8($sp)
	lw $t2, 12($sp)
	lw $t3, 16($sp)
	lw $t4, 20($sp)
	lw $t5, 24($sp)
	lw $t6, 28($sp)
	lw $ra, 32($sp)
	addi $sp, $sp, 36
	
	move $s4, $v0
	move $s5, $v1
	li $s7, 0		 # s7 contador
bfsFor:
	bge $s7, $s5, bfsContinue	# para cada sucesor
	#addi $t8, $t7, -1
	sll $t8, $s7, 2
	add $t8, $s4, $t8
	lw $s6, 0($t8)			# $s6 es el sucesor
	beq $s3, $s6, bfsWhileReturn	# si el sucesor es la meta, terminamos
bfsForSuccesor:
	addi $sp, $sp, -36
	sw $ra, 32($sp)
	sw $t6, 28($sp)
	sw $t5, 24($sp)
	sw $t4, 20($sp)
	sw $t3, 16($sp)
	sw $t2, 12($sp)
	sw $t1, 8($sp)
	sw $t0, 4($sp)
	
	move $a0, $t3
	move $a1, $t4
	move $a2, $t6
	jal cargarDistancias		# asigna sucesor.g y sucesor.h
	
	lw $t0, 4($sp)
	lw $t1, 8($sp)
	lw $t2, 12($sp)
	lw $t3, 16($sp)
	lw $t4, 20($sp)
	lw $t5, 24($sp)
	lw $t6, 28($sp)
	lw $ra, 32($sp)
	addi $sp, $sp, 36
	
	move $t3, $v0
	move $t4, $v1
	
	addi $sp, $sp, -36
	sw $ra, 32($sp)
	sw $t6, 28($sp)
	sw $t5, 24($sp)
	sw $t4, 20($sp)
	sw $t3, 16($sp)
	sw $t2, 12($sp)
	sw $t1, 8($sp)
	sw $t0, 4($sp)
	
	move $a0, $t2
	move $a1, $t3
	move $a2, $t4
	jal cargarF		# asigna sucesor.f
	
	lw $t0, 4($sp)
	lw $t1, 8($sp)
	lw $t2, 12($sp)
	lw $t3, 16($sp)
	lw $t4, 20($sp)
	lw $t5, 24($sp)
	lw $t6, 28($sp)
	lw $ra, 32($sp)
	addi $sp, $sp, 36
	
	move $t2, $v0
	
bfsContinue:
	cond1a:
		addi $sp, $sp, -36
		sw $ra, 32($sp)
		sw $t6, 28($sp)
		sw $t5, 24($sp)
		sw $t4, 20($sp)
		sw $t3, 16($sp)
		sw $t2, 12($sp)
		sw $t1, 8($sp)
		sw $t0, 4($sp)
	
		move $a0, $t1
		move $a1, $s6
		move $a2, $s1
		jal buscarElemento
	
		lw $t0, 4($sp)
		lw $t1, 8($sp)
		lw $t2, 12($sp)
		lw $t3, 16($sp)
		lw $t4, 20($sp)
		lw $t5, 24($sp)
		lw $t6, 28($sp)
		lw $ra, 32($sp)
		addi $sp, $sp, 36
		beq $v0, 1, cond1b
		bne $v0, 1, cond2a
	cond1b:
		addi $t7, $s6, -1
		sll $t7, $t7, 2
		add $t7, $t2, $t7	
		lw $t8, 0($t7)		# buscar sucesor.f
		
		addi $t7, $t6, -1
		sll $t7, $t7, 2
		add $t7, $t2, $t7	
		lw $t9, 0($t7) 		# buscar actual.f
		
		blt $t9, $t8, act1	# si actual.f < sucesor.f, se saca actual de la lista cerrada
		bge $t9, $t8, cond2a
	cond2a:
		addi $sp, $sp, -36
		sw $ra, 32($sp)
		sw $t6, 28($sp)
		sw $t5, 24($sp)
		sw $t4, 20($sp)
		sw $t3, 16($sp)
		sw $t2, 12($sp)
		sw $t1, 8($sp)
		sw $t0, 4($sp)
	
		move $a0, $t0
		move $a1, $t3
		move $a2, $t4
		jal buscarElemento
	
		lw $t0, 4($sp)
		lw $t1, 8($sp)
		lw $t2, 12($sp)
		lw $t3, 16($sp)
		lw $t4, 20($sp)
		lw $t5, 24($sp)
		lw $t6, 28($sp)
		lw $ra, 32($sp)
		addi $sp, $sp, 36
		beq $v0, 1, cond2b
		bne $v0, 1, cond3
	cond2b:
		addi $t7, $s6, -1
		sll $t7, $t7, 2
		add $t7, $t2, $t7	
		lw $t8, 0($t7)		# buscar sucesor.f
		
		addi $t7, $t6, -1
		sll $t7, $t7, 2
		add $t7, $t2, $t7	
		lw $t9, 0($t7) 		# buscar actual.f
		
		blt $t8, $t9, act1	# si actual.f < sucesor.f, se saca actual de la lista abierta
		bge $t8, $t9, cond3
	cond3:
		j act2
	act1:
		addi $t7, $s6, -1
		sll $t7, $t7, 2
		add $t7, $t3, $t7	
		lw $t8, 0($t7)		# sucesor.g = actual.g
	
		addi $sp, $sp, -36
		sw $ra, 32($sp)
		sw $t6, 28($sp)
		sw $t5, 24($sp)
		sw $t4, 20($sp)
		sw $t3, 16($sp)
		sw $t2, 12($sp)
		sw $t1, 8($sp)
		sw $t0, 4($sp)
	
		move $a0, $t5
		move $a1, $t6
		move $a2, $s1
		jal guardarElemento		# se saca 'actual' de la lista de nodos abiertos
		
		lw $t0, 4($sp)
		lw $t1, 8($sp)
		lw $t2, 12($sp)
		lw $t3, 16($sp)
		lw $t4, 20($sp)
		lw $t5, 24($sp)
		lw $t6, 28($sp)
		lw $ra, 32($sp)
		addi $sp, $sp, 36
	
		move $t5, $v0
		j condOut
	act2:
		addi $sp, $sp, -36
		sw $ra, 32($sp)
		sw $t6, 28($sp)
		sw $t5, 24($sp)
		sw $t4, 20($sp)
		sw $t3, 16($sp)
		sw $t2, 12($sp)
		sw $t1, 8($sp)
		sw $t0, 4($sp)
	
		move $a0, $t0
		move $a1, $s6
		move $a2, $s1
		jal guardarElemento		# se saca 'actual' de la lista de nodos abiertos
		
		lw $t0, 4($sp)
		lw $t1, 8($sp)
		lw $t2, 12($sp)
		lw $t3, 16($sp)
		lw $t4, 20($sp)
		lw $t5, 24($sp)
		lw $t6, 28($sp)
		lw $ra, 32($sp)
		addi $sp, $sp, 36
	
		move $t5, $v0
		j condOut
	condOut:
		addi $s7, $s7, 1
		j bfsFor
bfsWhileReturn:
	j bfsWhileCheck
bfsExit:
	move $v0, $t5
	jr $ra


################ cargarF #######################
# Calcula el nodo.f con la suma de nodo.g y nodo.h
 
# Entradas:
# $a0: listaf
# $a1: listag
# $a2: listah

# Salidas:
# $v0: listaf actualizada
###########################################################
cargarF:
	la $t0, 0($a0)		# $t0 listaf
	la $t1, 0($a1)		# $t1 listag
	la $t2, 0($a2)		# $t2 listah
cfLoop:
	addi $t4, $s6, -1
	sll $t4, $t4, 2
	add $t5, $t0, $t4
	add $t6, $t1, $t4
	add $t7, $t2, $t4
	lw $t8, 0($t6)
	lw $t9, 0($t7)
	add $t3, $t8, $t9
	sw $t3, 0($t5)
cfExit:
	move $v0, $t0
	jr $ra


################ cargarDistancias #######################
# Calcula nodo.g (distancia recorrida) y nodo.h (heur�stica de lo que falta por recorrer)
 
# Entradas:
# $a0: listag
# $a1: listah
# $a2: padre

# Salidas:
# $v0: listag
# $v1: listah
###########################################################
cargarDistancias:
	move $t0, $s6		# $t0 nodo
	la $t1, 0($a0)		# $t1 listag
	la $t2, 0($a1)		# $t2 listah
	move $t3, $a2		# $t3 padre
	
	addi $t4, $t3, -1
	sll $t4, $t4, 2
	add $t4, $t1, $t4
	lw $t5, 0($t4)		# $t5 padre.g

cdFor:
	addi $t7, $t0, -1
	sll $t4, $t7, 2
	add $t4, $t1, $t4
	
	addi $sp, $sp, -36
	sw $ra, 32($sp)
	sw $t6, 28($sp)
	sw $t5, 24($sp)
	sw $t4, 20($sp)
	sw $t3, 16($sp)
	sw $t2, 12($sp)
	sw $t1, 8($sp)
	sw $t0, 4($sp)
	
	move $a0, $s0	
	move $a1, $s1
	move $a2, $s5
	move $a3, $t0
	jal obtenerDistancia	# se obtiene la distancia entre el nodo y su padre
	
	lw $t0, 4($sp)
	lw $t1, 8($sp)
	lw $t2, 12($sp)
	lw $t3, 16($sp)
	lw $t4, 20($sp)
	lw $t5, 24($sp)
	lw $t6, 28($sp)
	lw $ra, 32($sp)
	addi $sp, $sp, 36

	add $t8, $t5, $v0
	sw $t8, 0($t4)		# nodo.g = padre.g + distancia entre padre y nodo
	
	addi $t7, $t0, -1
	sll $t4, $t7, 2
	add $t4, $t2, $t4
	
	addi $sp, $sp, -36
	sw $ra, 32($sp)
	sw $t6, 28($sp)
	sw $t5, 24($sp)
	sw $t4, 20($sp)
	sw $t3, 16($sp)
	sw $t2, 12($sp)
	sw $t1, 8($sp)
	sw $t0, 4($sp)
	
	move $a0, $s0	
	move $a1, $s1
	move $a2, $t0
	jal heuristica		# se calcula la heuristica y se guarda en nodo.h
	
	lw $t0, 4($sp)
	lw $t1, 8($sp)
	lw $t2, 12($sp)
	lw $t3, 16($sp)
	lw $t4, 20($sp)
	lw $t5, 24($sp)
	lw $t6, 28($sp)
	lw $ra, 32($sp)
	addi $sp, $sp, 36
	
	move $t9, $v0
	sw $t9, 0($t4)		# nodo.h = heuristica(nodo)
	
cdExit:
	move $v0, $t1
	move $v1, $t2
	jr $ra
	
	

################ generarSucesores #######################
# Entrega una lista con los sucesores de un nodo
 
# Entradas:
# $a0: Matriz de adyacencia
# $a1: Largo de la lista
# $a2: Lista de sucesores
# $a3: Nodo Generatriz

# Salidas:
# $v0: Lista de sucesores
# $v1: Largo de lista de sucesores
###########################################################
generarSucesores:
	la $t0, 0($a0)		# $t0 es un puntero fijo a la posicion inicial a la matriz de adyacencia
	la $t1, 0($a2)		# $t1 es un puntero fijo a la posicion inicial de la lista de sucesores
	lw $t2, 0($a1)		# $t2 es la cantidad de nodos (condici�n de borde)
	move $t3, $a3		# $t3 es el nodo al cual se generar�n los sucesores
	li $t4, 0		# $t4 es un contador de sucesores
gsAllocate:
	addi $t6, $t3, -1
	mul $t6, $t2, $t6	# $t6 = nodo a buscar * cantidad de nodos
	li $t7, 0		# contador
gsLoop:
	bge $t7, $t2, gsExit	# Abortar al terminar la lista
	add $t8, $t6, $t7	# buscar sucesor
	sll $t8, $t8, 2
	add $t8, $t0, $t8
	lw $t9, 0($t8)
	bgt $t9, 0, gsCheck	# si el elemento es sucesor, guardar en la lista
gsIncrement:
	addi $t7, $t7, 1
	j gsLoop
gsCheck:
	addi $sp, $sp, -40
	sw $ra, 36($sp)
	sw $t7, 32($sp)
	sw $t6, 28($sp)
	sw $t5, 24($sp)
	sw $t4, 20($sp)
	sw $t3, 16($sp)
	sw $t2, 12($sp)
	sw $t1, 8($sp)
	sw $t0, 4($sp)
	
	move $a0, $t1
	addi $a1, $t7, 1
	jal guardarElemento	# guardar sucesor

	lw $t0, 4($sp)
	lw $t1, 8($sp)
	lw $t2, 12($sp)
	lw $t3, 16($sp)
	lw $t4, 20($sp)
	lw $t5, 24($sp)
	lw $t6, 28($sp)
	lw $t7, 32($sp)
	lw $ra, 36($sp)
	addi $sp, $sp, 40
	move $t1, $v0
	addi $t4, $t4, 1
	j gsIncrement
gsExit:
	move $v0, $t1
	move $v1, $t4
	jr $ra	
	
################ encontrarMenorAbierto #######################
# Entrega el nodo abierto con el menor valor de la lista
 
# Entradas:
# $a0: Lista de nodos abiertos
# $a1: Largo de la lista
# $a2: Lista de valores

# Salidas:
# $v0: Menor nodo
###########################################################
encontrarMenorAbierto:
	la $t0, 0($a0)		# $t0 es un puntero fijo a la posicion inicial de la lista de nodos abiertos
	la $t1, 0($a2)		# $t1 es un puntero fijo a la posicion inicial de la lista de valores
	la $t2, 0($a0)		# $t2 es un puntero movil de la lista de nodos abiertos
	la $t3, 0($a2)		# $t3 es un puntero movil de la lista de valores
	lw $t4, 0($a1)		# $t4 es la cantidad de nodos (condici�n de borde)
	li $t5, 0		# $t5 es un contador
	li $t6, 0		# $t6 es el valor a retornar
	lw $t7, infinito	# $t7 es un comparador	
emaLoop:
	bge $t5, $t4, emaExit
	sll $t2, $t5, 2		# buscar elemento
	add $t2, $t0, $t2
	lw $t8, 0($t2)
	bgt $t8, -1, emaCheck	# si el elemento no es vacio, chequear
	j emaCounter
emaCheck:
	sll $t3, $t8, 2		
	add $t3, $t1, $t3
	lw $t9, 0($t3)
	ble $t9, $t7, emaIf	# si el elemento.valor es menor que el comparador, asignamos ese nodo como menor
emaCounter:
	addi $t5, $t5, 1
	j emaLoop	
emaIf:
 	move $t6, $t8		# asignar menor nodo
	move $t7, $t9
	j emaCounter
emaExit:
	move $v0, $t6
	jr $ra
	
	

################ encontrarMenorNodo #######################
# Entrega el nodo con el menor valor de la lista
 
# Entradas:
# $a0: Lista (usualmente 'listaf')
# $a1: Largo de la lista

# Salidas:
# $v0: Menor nodo
###########################################################
encontrarMenorNodo:
	la $t0, 0($a0)		# $t0 es un puntero fijo a la posicion inicial de la matriz
	lw $t1, 0($a1)		# $t1 es la cantidad de nodos (condici�n de borde)
	move $t2, $t0		# $t2 es un puntero movil
	li $t3, 0		# $t3 es un contador
	li $t4, 0		# $t4 es el valor a retornar
	lw $t5, infinito	# $t5 es un comparador
emnLoop:
	bge $t3, $t1, emnExit
	sll $t2, $t3, 2
	add $t2, $t0, $t2
	lw $t6, 0($t2)
	addi $t3, $t3, 1
	beq $t6, -1, emnLoop
	blt $t6, $t5, emnIf
	j emnLoop
emnIf:
	move $t5, $t6
 	addi $t7, $t3, 1
	move $t4, $t7
	j emnLoop
emnExit:
	move $v0, $t4
	jr $ra

#################### iniciarArreglo #######################
# inicializa un arreglo con el tama�o indicado
 
# Entradas:
# $a0: Tama�o del Arreglo

# Salidas:
# $v0: Arreglo inicializado
###########################################################
iniciarArreglo:
	addi $a0, $a0, 1
	li $v0, 9
	syscall
	move $t0, $v0
	subi $t1, $a0, 1
	sll $t2, $t1, 2
	add $t3, $t0, $t2
	li $t4, -1
	sw $t4, 0($t3)
	move $v0, $t0
	jr $ra


##################### imprimirNodos #######################
# Funcion que permite mostrar un vector cualquiera
 
# Entradas:
# $a0: Direccion base del vector.
# $a1: Tama�o del vector.

# Salidas:
# Esta funcion no devuelve nada.
###########################################################
imprimirNodos:
	la $t0, 0($a0)		# $t0 es un puntero fijo a la posicion inicial de la matriz
	move $t1, $a1		# $t1 es la cantidad de nodos (condici�n de borde)
	move $t2, $t0		# $t2 es un puntero movil
	li $t3, 0		# $t3 es un contador
f1Loop:   
	bge $t3, $t1, f1Out
        # imprimir elemento
        lw $a0, 0($t2)    # Carga a $a0 el contenido de la direccion $t2
        li $v0, 1
        syscall
        # imprimir espacio
        bge $t3, $t1, f1Out
        la $a0, espacio
	li $v0, 4
        syscall
        # aumentar contador
	addi $t3, $t3, 1
        sll $t4, $t3, 2
	add $t2, $t0, $t4
        j f1Loop
f1Out:
	jr $ra


############## rellenarListaNodos ###############
# Rellena el arreglo ingresado con nodos consecutivos
# (1, 2, 3, ...)
 
# Entradas:
# $a0: Direccion base del arreglo
# $a1: Tama�o del arreglo

# Salidas:
# $v0: Lista de nodos rellena
#################################################
rellenarListaNodos:
	la $t0, 0($a0)		# $t0 es un puntero fijo a la posicion inicial de la matriz
	lw $t1, 0($a1)		# $t1 es la cantidad de nodos
	move $t2, $t0		# $t2 es un puntero movil
	li $t3, 1		# $t3 es contador
	li $t4, 0 		# $t4 es un asignador de memoria
rlnLoop:
	bgt $t3, $t1, rlnExit
	sll $t5, $t4, 2
	add $t2, $t0, $t5
	sw $t3, 0($t2)
	addi $t3, $t3, 1
	addi $t4, $t4, 1
	j rlnLoop
rlnExit:
	move $v0, $t0
	jr $ra	


################ obtenerDistancia ######################
# Dada una matriz de adyacencia, un nodo de entrada y uno de salida
# obtener la distancia entre ambos nodos
 
# Entradas:
# $a0: Direccion base de la matriz
# $a1: Cantidad de nodos en el grafo
# $a2: Grafo de origen
# $a3: grafo de destino

# $v0: distancia al nodo
#####################################################
obtenerDistancia:
	la $t0, 0($a0)		# $t0 es un puntero fijo a la posicion inicial de la matriz
	lw $t1, 0($a1)		# $t1 es la cantidad de nodos
	move $t2, $a2		# $t2 es el nodo desde cual parte la busqueda
	move $t3, $a3		# $t3 es el nodo al cual queremos llegar
odLoop:
	addi $t4, $t2, -1
	addi $t5, $t3, -1
	mul $t6, $t1, $t4
	add $t7, $t6, $t5
	sll $t8, $t7, 2
	add $t9, $t0, $t8
	lw $v0, 0($t9)
odEnd:
	jr $ra


############## minimaDistancia ######################
# retorna el nodo cuya distancia es m�nima al nodo actual
 
# Entradas:
# $a0: Direccion base de la matriz
# $a1: Cantidad de nodos en el grafo
# $a2: Nodo a calcular

# Salidas:
# $v0: nodo cuya distancia es minima
# $v1: distancia a ese nodo
#####################################################
minimaDistancia:
	move $s0, $a0		# $s0 es un puntero fijo a la posicion inicial de la matriz
	move $s1, $a1		# $s1 es la cantidad de nodos
	move $s2, $a2		# $s2 es el nodo desde cual parte la busqueda
	lw $s3, infinito	# $s3 es el m�nimo actual
	li $v0, 0
	bgt $s2, $s1, mdError
mdLoop:
	li $s4, 1	# $s4 contador del for
	lw $s5, 0($s1)	# condici�n de borde
	mdFor:
		bgt $s4, $s5, mdReturn
		
		move $a0, $s0
		move $a1, $s1
		move $a2, $s2
		move $a3, $s4
		
		addi $sp, $sp, -40
		sw $ra, 36($sp)
		sw $s0, 32($sp)
		sw $s1, 28($sp)
		sw $s2, 24($sp)
		sw $s3, 20($sp)
		sw $s4, 16($sp)
		sw $s5, 12($sp)
		sw $s6, 8($sp)
		sw $s7, 4($sp)
	
		jal obtenerDistancia
		
		lw $s7, 4($sp)
		lw $s6, 8($sp)
		lw $s5, 12($sp)
		lw $s4, 16($sp)
		lw $s3, 20($sp)
		lw $s2, 24($sp)
		lw $s1, 28($sp)
		lw $s0, 32($sp)
		lw $ra, 36($sp)
		addi $sp, $sp, 40
				
		move $t1, $v0
		beqz $t1, mdElse
		ble $t1, $s3, mdIf
		bgt $t1, $s3, mdElse
	mdIf:
		move $s3, $t1
		move $s6, $s4
		move $s7, $t1
	mdElse:
		addi $s4, $s4, 1
		j mdFor
mdReturn:
	move $v0, $s6
	move $v1, $s7
        jr $ra
mdError:
	la $a0, errortext
	li $v0, 4
        syscall
      	li $v0, 0
        jr $ra


############## verificarListaVacia #############
# Verifica si la lista de nodos est� vac�a
# 
 
# Entradas:
# $a0: Direccion base de la lista
# $a1: Cantidad de nodos en la lista

# Salidas:
# $v0: Entrega 0 si la lista tiene elementos y 1 si la lista est� vac�a
################################################
verificarListaVacia:
	la $t0, 0($a0)		# $t0 es un puntero fijo a la posicion inicial de la matriz
	lw $t1, 0($a1)		# $t1 es la cantidad de nodos
	li $t2, 0		# $t2 es un contador de nodos
	move $t3, $a0		# $t3 es un puntero m�vil
	li $t6, 0		# $t6 es el resultado de retorno
vlvLoop:
	sll $t4, $t2, 2
	add $t3, $t0, $t4
	lw $t5, 0($t3)
	addi $t2, $t2, 1
		
	bne $t5, -1, vlvReturnFalse
	bgt $t2, $t1, vlvReturnTrue
	j vlvLoop
vlvReturnFalse:
	li $t6, 0
	j vlvExit
vlvReturnTrue:
	li $t6, 1
	j vlvExit
vlvExit:
	move $v0, $t6
	jr $ra

############## heuristica ######################
# Funci�n de Heur�stica para BFS/A*
# Consiste en sumar el peso de todos los caminos sucesores del nodo a calcular
 
# Entradas:
# $a0: Direccion base de la matriz
# $a1: Cantidad de nodos en el grafo
# $a2: Nodo a calcular

# Salidas:
# $v0: Resultado de la Heur�stica
################################################
heuristica:
	move $t0, $a0		# $s0 es un puntero fijo a la posicion inicial de la matriz
	move $t1, $a1		# $s1 es la cantidad de nodos
	move $t2, $a2		# $s2 es el nodo a buscar
	li $t3, 0		# $s3 es la suma de los nodos
	li $v0, 0
	bgt $t2, $t1, heuError
heuLoop:
	li $t4, 1	# $s4 contador del for
	lw $t5, 0($s1)	# condici�n de borde
	heuFor:
		bgt $t4, $t5, heuReturn
		
		move $a0, $t0
		move $a1, $t1
		move $a2, $t2
		move $a3, $t4
		
		addi $sp, $sp, -32
		sw $ra, 28($sp)
		sw $t0, 24($sp)
		sw $t1, 20($sp)
		sw $t2, 16($sp)
		sw $t3, 12($sp)
		sw $t4, 8($sp)
		sw $t5, 4($sp)
		
		jal obtenerDistancia
		
		lw $t5, 4($sp)
		lw $t4, 8($sp)
		lw $t3, 12($sp)
		lw $t2, 16($sp)
		lw $t1, 20($sp)
		lw $t0, 24($sp)
		lw $ra, 28($sp)
		addi $sp, $sp, 32
		
		move $t6, $v0
		add $t3, $t6, $t3
		addi $t4, $t4, 1
		j heuFor
heuReturn:
	move $v0, $t3
        jr $ra
heuError:
	la $a0, errortext
	li $v0, 4
        syscall
      	li $v0, 0
        jr $ra


############### guardarElemento  #######################
# Funcion que permite extraer 
 
# Entradas:
# $a0: Direccion base del vector a ingresar.
# $a1: Elemento a guardar
# $a2: Largo del vector


# Salidas:
# $v0: Vector con el nodo agregado
# $v1: Retorna 1 si se encontr� el elemento y 0 si no se encontr�
###########################################################
guardarElemento:
	la $t0, 0($a0)		# $t0 puntero fijo al inicio del vector
	lw $t1, 0($a2)		# $t1 es el largo del vector
        move $t2, $t0 		# $t2 puntero movil al vector
        move $t3, $a1 		# $t3 es el elemento a guardar
        li $t4, 0		# $t4 contador de elementos
        li $t5, 0		# $t5 check de elemento encotrado
geCheck:
        lw $t6, 0($t2)	   	# $t6 contenido del puntero movil
	beq $t6, -1, geReplace	# si el elemento es -1, guardar en esa posicion
	sll $t7, $t4, 2		# sino, aumentar el contador para ir al siguiente elemento
	add $t2, $t0, $t7
	addi $t4, $t4, 1
	bgt $t4, $t1, geExit	# si termina el arreglo, salimos del programa
   	j geCheck
geReplace:
	sw $t3, 0($t2)		# se guarda el elemento
	li $t5, 1		# se cambia el check a encontrado
geExit:
	move $v0, $t0
	move $v1, $t5
	jr $ra


################# extraerElemento  #######################
# Funcion que permite extraer un elemento de un arreglo
 
# Entradas:
# $a0: Direccion base del vector a ingresar.
# $a1: Elemento a extraer
# $a2: Largo del vector


# Salidas:
# $v0: Vector con el nodo eliminado
# $v1: Retorna 1 si se encontr� el elemento y 0 si no se encontr�
###########################################################
extraerElemento:
	la $t0, 0($a0)		# $t0 puntero fijo al inicio del vector
	lw $t1, 0($a2)		# $t1 es el largo del vector
        move $t2, $t0 		# $t2 puntero movil al vector
        move $t3, $a1 		# $t3 es el elemento a extraer
        li $t4, 0		# $t4 contador de elementos
        li $t5, 0		# $t5 check de elemento encotrado
eeCheck:
        lw $t6, 0($t2)	   	# $t6 contenido del puntero movil
	beq $t3, $t6, eeReplace	# si el elemento se encuentra, reemplazarlo por -1
	sll $t7, $t4, 2		# sino, aumentar el contador para ir al siguiente elemento
	add $t2, $t0, $t7
	addi $t4, $t4, 1
	bgt $t4, $t1, eeExit	# si termina el arreglo, salimos del programa
   	j eeCheck
eeReplace:
	li $t8, -1
	sw $t8, 0($t2)		# se reemplaza el elemento por -1
	li $t5, 1		# se cambia el check a encontrado
eeExit:
	move $v0, $t0
	move $v1, $t5
	jr $ra


################# buscarElemento  #######################
# Funcion que permite buscar un elemento en un arreglo
 
# Entradas:
# $a0: Direccion base del vector a ingresar.
# $a1: Elemento a buscar
# $a2: Largo del vector


# Salidas:
# $v0: Entrega 1 si el elemento se encuentra y 0 si no se encuentra
# $v1: Posicion del elemento (indexada en 1)
###########################################################
buscarElemento:
	la $t0, 0($a0)		# $t0 puntero fijo al inicio del vector
	lw $t1, 0($a2)		# $t1 es el largo del vector
        move $t2, $t0 		# $t2 puntero movil al vector
        move $t3, $a1 		# $t3 es el elemento a extraer
        li $t4, 0		# $t4 contador de elementos
        li $t5, 0		# $t5 check de elemento encotrado
beCheck:
	sll $t7, $t4, 2
	add $t2, $t0, $t7
        lw $t6, 0($t2)	   	# $t6 contenido del puntero movil
	beq $t3, $t6, beReplace	# si el elemento se encuentra, marcar check
	addi $t4, $t4, 1	# sino, aumentar el contador para ir al siguiente elemento
	bgt $t4, $t1, beExit	# si termina el arreglo, salimos del programa
   	j beCheck
beReplace:
	li $t5, 1		# se cambia el check a encontrado
beExit:
	move $v0, $t5
	move $v1, $t4
	jr $ra
	
### salida del programa ###
exit:
	
