#datos iniciales
.data
# Matriz de adyacencia expresado en una fila (-1 significa fin de la matriz)
grafo1:	.word	0, 2, 9, 0, 0, 30, 7, 0, 10, 15, 0, 0, 9, 10, 0, 11, 0, 2, 0, 15, 11, 0, 6, 0, 0, 0, 0, 6, 0, 9, 14, 0, 2, 0, 9, 0, -1
# Cantidad de grafos de la matriz (cada cuanto debo cortar la fila grafo1)
grafo2: .word	6
#nodo de inicio  del camino
nodoInicial:	.word	1
#nodo destino, es decir donde quiero llegar
nodoFinal:	.word   5
#vector que guardara los caminos  del vector
    elemGuarCami: .word 0, 0, 0, 0, 0   
#numero de elementos del vector de caminos    
    elemvectCami:   .word 5
#vector que guardara los nodos ya recorriddos del grafo
    elemGuarNodo: .word 0, 0, 0, 0, 0 
#numero de elementos del vector de nodos
    elemvectNodo: .word 5

.text 

main:



#los retornos de esta funcion estar�n en $vo,$v1
li $s0,-1 
la $s5, nodoFinal
lw $s5, 0($s5)

pregunta: beq $s0,$s5, imprimir 
	  bne $s0,$s5, for
	  
for:

#llamada de la funcion que  busca el camino minimo  en el nodo 
	la $a0,grafo1	
	la $a1,grafo2
	la $a2, nodoInicial
	la $a3, nodoFinal	#este nodo jamas cambiara a lo largo de la ejecucion
	jal minimaDistancia
	move $s0, $v0
	move $s1, $v1
	
# Invocacion de la funcion para verificar caminos ya recorridos en un vector
   	la $a0,elemGuarNodo
   	la $a1, elemvectNodo
   	move $a2, $s1
   	jal verificar
   	move $s3,$v0
   	li $t1, 1
   	
  seUso: bne $s3,$t1, no
  	 beq $s3, $s1, si

no:
 # Invocacion de la funcion para guardar caminos ya recorridos en un vector, en el primero se guarda el peso
	la $a0, elemGuarCami
	move $a1, $s0
	jal guardar
 # Invocacion de la funcion para guardar caminos ya recorridos en un vector, aqui se guarda el nodo	
	la $a0, elemGuarNodo
	move $a1, $s1
	jal guardar
#invocacion de  funcion para  hacer 0 los nodos ya usados
	la $a0,grafo1
	la $a1,grafo2
	la $a2, nodoFinal
	la $a3, nodoInicial
	jal hacerCeroLosCaminosYaUsados
#se cambiara el nodo  que ya se recorrio por el nodo inicial	
	la  $s4, nodoInicial
	sw $s1, ($s4)
	move $s0, $s1
	j pregunta

	si:
#invocacion de  funcion para  hacer 0 los nodos ya usados
	la $a0,grafo1
	la $a1 ($s3)
	la $a2, nodoFinal
	la $a3, nodoInicial
	jal hacerCeroLosCaminosYaUsados	
	j for

#cuando se llega al nodo destino se ha de llamar a la funcion  sumar que presentara la suma de los caminos mas cortos,  en conjunto con la etiqueta imprimir

imprimir:
	la $a0,elemGuarCami
   	la $a1, elemvectCami
   	jal sumar
   	move $t0,$v0
   	move $a0, $t0
        li $v0,1
        syscall
#una vez impreso la suma de los caminos se ha de  ir al final
j end


##################### minimaDistancia #######################
# funcion de retorna el camino con menos peso y el peso al nodo deseado
 
# Entradas:
# $a0: Direccion base del vector.
# $a1: Tama�o del vector.
# $a2: nodo inicial
# $a3: nodo final

# Salidas:
# no retorna
###########################################################

minimaDistancia:

		li $t0, 1	        # t0 es el acumulador
        	la $t1, ($a0)		#t1 puntero fijo 
        	move $t2, $t1 		# t2 puntero movil
        	lw $t3,0($a1)		# numero de elementos del vector
		lw $t4, 0($t2)	   	#contenido del puentero movil
        	lw $t5,0($a2)		# nodo inicial, nodo de inicio
        	lw $t6,0($a3)		#nodo final

#direccionamiento inicial del los punteros

subiu  $t5,$t5,1			#resta uno a la posicion del array al que se accedera
mul    $t5, $t5,$t3			#lo multiplica por el nuemero de elementos del arreglo
sll    $t7, $t5, 2			#se multiplica por 4  para alinear la memoria
add    $t2, $t1,$t7			#suma las pociciones a  mover en el puntero movil  
lw     $t4, 0($t2)			#carga el contenido del puntero movil


for1: beq $t0,$t3, finLoop
      bne $t0,$t3, loop
     
loop: addi $t5, $t5, 1
      addi $t0, $t0, 1
      sll  $t7, $t5, 2
      add  $t2, $t1,$t7
      lw   $t8,0($t2)
      beq $t8,$zero,loop
      beq $t4,$zero,reemplazar      
      ble $t8,$t4,reemplazar 
      bgt $t8,$t4,for1

reemplazar:
		move $t4,$t8
		move $t9,$t0
		j for1

finLoop: 
	move $a0, $t4
        li $v0,1
        syscall
        move $a0, $t9
        li $v0,1
        syscall
	move $v0, $t4
	move $v1, $t9
	jr $ra 
	
    ###################### Guardar       ######################
    # Funcion que permite guardar los caminos recorridos
     
    # Entradas:
    # $a0: Direccion base del vector a ingresar.
    # $a1: elemento a guardar
    # Salidas:
    # Esta funcion no devuelve nada.
    ###########################################################
  guardar:
            li $t0, 1	        	# t0 es el acumulador
            la $t1, ($a0)		#t1 puntero 
            move $t2, $t1 		# t2 puntero movil
            lw $t4, 0($t2)	   	#contenido del puentero movil
      	       
    preg:
   	 bnez $t4, correr
	 sw $a1, 0($t2)
         lw $a0,($t2) 
         li $v0,1
         syscall
         jr $ra 
     correr:
    	sll $t3,$t0,2
	add $t2,$t1,$t3
	lw $t4, ($t2)	
	move $a0,$t4	# Carga a $a0 el contenido de la direccion $t2
	li $v0,1
	syscall
	addi $t0,$t0,1  
	
	
	 ###################### Verificar      ######################
    # Funcion que verifica si un camino ya fue recorrido 
     
    # Entradas:
    # $a0: Direccion base del vector a ingresar.
    # $a1: camino a verificar 
    # Salidas:
    # Esta funcion no devuelve nada.
    ###########################################################
 verificar:
 	li $t0, 1	        # t0 es el acumulador
        la $t1, ($a0)		#t1 puntero fijo 
        move $t2, $t1 		# t2 puntero movil
        lw $t3,0($a1)		# numero de elementos del vector
        lw $t4, 0($t2)	   	#contenido del puentero movil
        move $t6,$a2		#elemento a comprar 
 
 consulta:
 	  beq $t6,$t4, esta 
 	  beq $t3, $t0, noEsta
 	  bne $t0,$t3,mover
 esta: 
 	li $t7, 1
 	move $v0,$t7
 	jr $ra
 
 mover:
 	sll $t5,$t0,2
	add $t2,$t1,$t5
	lw $t4,0($t2)	
	move $a0,$t4	# Carga a $a0 el contenido de la direccion $t2
	li $v0,1
	syscall
	addi $t0,$t0,1
	j consulta  
 noEsta:
 	li $t7, 0
 	move $v0,$t7
	jr $ra
#################### hacerCeroLosCaminosYaUsados #######################
# vuelve 0 el peso del nodo ya recorrido

# Entradas:
# $a0: Direccion base del vector.
# $a1: Tama�o del vector.
# $a2: nodo que se debe volver 0
# $a3 nodo  actual

  
# Salidas:
# Esta funcion no devuelve nada.
###########################################################
hacerCeroLosCaminosYaUsados:

		li $t0, 1	        # t0 es el acumulador
        	la $t1, ($a0)		#t1 puntero fijo 
        	move $t2, $t1 		# t2 puntero movil
        	lw $t3,0($a1)		# numero de elementos del vector
        	lw $t4,0($a2)		# nodo actual, nodo alque se llego
        	lw $t5,0($a3)		# camino que se debe volver 0
#direccionamiento inicial del los punteros

subiu  $t4,$t4,1			#resta uno a la posicion del array al que se accedera
mul    $t4, $t4,$t3			#lo multiplica por el nuemero de elementos del arreglo
sll    $t6, $t4, 2			#se multiplica por 4  para alinear la memoria
add    $t2, $t1,$t6			#suma las pociciones a  mover en el puntero movil  
 

pregun: beq $t0, $t5, reemplazar1
      	  bne $t0, $t5, correr1
      
correr1: 
       addi $t4,$t4,1
       addi $t0,$t0,1
       sll  $t6,$t4,2
       add  $t2, $t1,$t6
       j pregunta
       
reemplazar1:
	li $t7, 5
        sw $t7, ($t2)
        move $v0,$t1
	jr $ra
 
 
     ###################### sumar     ######################
    # Funcion que verifica si un camino ya fue recorrido 
     
    # Entradas:
    # $a0: Direccion base del vector a ingresar.
    # $a1: camino a verificar 
    # Salidas:
    # la suma de los caminos  recorridos
    ###########################################################
sumar:
 	li $t0, 1	        # t0 es el acumulador
        la $t1, ($a0)		#t1 puntero fijo 
        move $t2, $t1 		# t2 puntero movil
        lw $t3,0($a1)		# numero de elementos del vector
        lw $t4, 0($t2)	   	#contenido del puentero movil
        li $t6,0		# inicializa el contenid de la suma en 0
 
 pregunt:
 	  beq $t3, $t0, ultimoComponente
 	  bne $t0,$t3,sum
 
 sum:
	lw $t4,0($t2)
	add $t6,$t6,$t4	
	sll $t5,$t0,2
	add $t2,$t1,$t5
	move $a0,$t6	# Carga a $a0 el contenido de la direccion $t2
	li $v0,1
	syscall
	addi $t0,$t0,1
	j pregunta   
ultimoComponente:
 	lw $t4,0($t2)
	add $t6,$t6,$t4
 	move $v0,$t6
	jr $ra
	
	end:
